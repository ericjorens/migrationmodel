package api.config;

public class DBConstants {
    public static String DB_HOST =  "jdbc:mysql://localhost:3306";
    public static String DB_NAME =  "migration_model";
    public static String DB_DRIVER =  "com.mysql.jdbc.Driver";
    public static String DB_USER =  "migrator";
    public static String DB_PASSWORD =  "migrator";
    public static String DB_USE_SSL =  "false";
    public static String DB_REQUIRED_SSL =  "false";
    public static String DB_SERVER_TIMEZONE =  "";
}
