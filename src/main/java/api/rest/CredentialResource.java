package api.rest;

import api.entity.Credential;
import api.service.BaseService;
import api.service.CredentialService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/credential")
@Produces(MediaType.APPLICATION_JSON)
public class CredentialResource extends BaseResource<Credential> {
    @EJB
    CredentialService service;

    @Override
    protected Credential getEntityInstance() {
        return new Credential();
    }

    @Override
    protected BaseService<Credential> getService() {
        return service;
    }

    @GET
    public Response getCredentials() {
        return this.getAllEntity();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCredential(@PathParam("id") Long id) {
        return this.getEntity(id);
    }

    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCredential(@PathParam("id") Long id) {
        return this.deleteEntity(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCredential(Credential c) {
        return this.addEntity(c.getId(), c);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCredentialById(Credential c) {
        return this.mergeEntity(c);
    }


}




