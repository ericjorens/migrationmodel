package api.rest;

import api.entity.Volume;
import api.service.BaseService;
import api.service.VolumeService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/volume")
@Produces(MediaType.APPLICATION_JSON)
public class VolumeResource extends BaseResource<Volume> {
    @EJB
    VolumeService service;

    @Override
    protected Volume getEntityInstance() {
        return new Volume();
    }

    @Override
    protected BaseService<Volume> getService() {
        return service;
    }

    @GET
    public Response getVolumes() {
        return this.getAllEntity();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVolume(@PathParam("id") Long id) {
        return this.getEntity(id);
    }

    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteVolume(@PathParam("id") Long id) {
        return this.deleteEntity(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addVolume(Volume v) {
        return this.addEntity(v.getId(), v);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateVolumeById(Volume v) {
        return this.mergeEntity(v);
    }


}




