package api.rest;

import api.entity.TargetCloud;
import api.service.BaseService;
import api.service.TargetCloudService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/targetcloud")
@Produces(MediaType.APPLICATION_JSON)
public class TargetCloudResource extends BaseResource<TargetCloud> {
    @EJB
    TargetCloudService service;

    @Override
    protected TargetCloud getEntityInstance() {
        return new TargetCloud();
    }

    @Override
    protected BaseService<TargetCloud> getService() {
        return service;
    }

    @GET
    public Response getTargetClouds() {
        return this.getAllEntity();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTargetCloud(@PathParam("id") Long id) {
        return this.getEntity(id);
    }

    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTargetCloud(@PathParam("id") Long id) {
        return this.deleteEntity(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addTargetCloud(TargetCloud t) {
        return this.addEntity(t.getId(), t);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTargetCloudById(TargetCloud t) {
        return this.mergeEntity(t);
    }


}




