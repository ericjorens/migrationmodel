package api.rest;

import api.entity.BaseEntity;
import api.service.BaseService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

public abstract class BaseResource<T extends BaseEntity> {
    public Gson gson = new Gson();

    protected abstract T getEntityInstance();

    protected abstract BaseService<T> getService();

    protected Response addEntity(Long id, T t) {
        try {
            T e = getService().addEntity(Collections.singletonList(t)).get(0);
            return Response.ok().entity(e).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    protected Response getAllEntity(){
        try {
            List<T> list = getService().getAll();
            return Response.ok().entity(list).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    protected Response getEntity(Long id) {
        try {
            T entity = getService().getById(id);
            return Response.ok().entity(entity).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    protected Response deleteEntity(Long id) {
        try {
            T entity = getService().getById(id);
            getService().deleteEntity(Collections.singletonList(entity));
            return Response.ok().entity(getJsonMsg("The entity was removed!")).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    protected Response mergeEntity(T entity) {
        try {
            T res = getService().mergeEntity(Collections.singletonList(entity)).get(0);
            return Response.ok().entity(res).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    public String getJsonMsg(String message){
        JsonObject j = new JsonObject();
        j.addProperty("message", message);
        return j.toString();
    }

}
