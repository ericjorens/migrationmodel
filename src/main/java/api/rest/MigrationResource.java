package api.rest;

import api.entity.Migration;
import api.entity.MigrationHistory;
import api.service.BaseService;
import api.service.MigrationHistoryService;
import api.service.MigrationService;
import com.google.gson.JsonObject;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/migration")
@Produces(MediaType.APPLICATION_JSON)
public class MigrationResource extends BaseResource<Migration> {
    @EJB
    MigrationService service;

    @EJB
    MigrationHistoryService migrationHistoryService;

    @Override
    protected Migration getEntityInstance() {
        return new Migration();
    }


    @Override
    protected BaseService<Migration> getService() {
        return service;
    }

    @GET
    public Response getMigrations() {
        return this.getAllEntity();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMigration(@PathParam("id") Long id) {
        return this.getEntity(id);
    }

    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMigration(@PathParam("id") Long id) {
        return this.deleteEntity(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addMigration(Migration m) {
        return this.addEntity(m.getId(), m);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMigrationById(Migration m) {
        return this.mergeEntity(m);
    }

    @Path("/history/{migrationId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLatestMigrationHistoryByMigrationId(@PathParam("migrationId") Long id) {
        try {
            MigrationHistory history = migrationHistoryService.getLatestByMigrationId(id);
            return Response.ok().entity(history).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    @Path("/history/{migrationId}/all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMigrationHistoryByMigrationId(@PathParam("migrationId") Long id) {
        try {
            List<MigrationHistory> history = migrationHistoryService.getAllByMigrationId(id);
            return Response.ok().entity(history).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    @Path("/{migrationId}/start")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response startMigration(@PathParam("migrationId") Long id) {
        try {
            MigrationHistory history = migrationHistoryService.startMigration(id);
            return Response.ok().entity(history).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }


    @Path("/{migrationHistoryId}/stop")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response endMigration(@PathParam("migrationHistoryId") Long id) {
        try {
            MigrationHistory history = migrationHistoryService.endMigration(id);
            return Response.ok().entity(history).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

    @Path("/{migrationHistoryId}/progress")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMigrationProgress(MigrationHistory migrationHistory) {
        try {
            Long percentage = migrationHistoryService.getProgressPercentage(migrationHistory);
            return Response.ok().entity(percentage).build();
        } catch (Exception e) {
            return Response.status(400).entity(getJsonMsg(e.getMessage())).build();
        }
    }

}




