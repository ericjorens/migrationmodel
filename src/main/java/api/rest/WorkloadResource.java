package api.rest;

import api.entity.Workload;
import api.service.BaseService;
import api.service.WorkloadService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/workload")
@Produces(MediaType.APPLICATION_JSON)
public class WorkloadResource extends BaseResource<Workload> {
    @EJB
    WorkloadService service;

    @Override
    protected Workload getEntityInstance() {
        return new Workload();
    }


    @Override
    protected BaseService<Workload> getService() {
        return service;
    }

    @GET
    public Response getWorkloads() {
        return this.getAllEntity();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkload(@PathParam("id") Long id) {
        return this.getEntity(id);
    }

    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteWorkload(@PathParam("id") Long id) {
        return this.deleteEntity(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addWorkload(Workload w) {
        return this.addEntity(w.getId(), w);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateWorkloadById(Workload w) {
        return this.mergeEntity(w);
    }


}




