package api.service;

import api.dao.BaseDAO;
import api.dao.VolumeDAO;
import api.dao.WorkloadDAO;
import api.entity.Volume;
import api.entity.Workload;
import com.sun.corba.se.spi.orbutil.threadpool.Work;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Stateless
public class WorkloadService extends BaseService<Workload> {

    @EJB
    WorkloadDAO dao;
    @EJB
    VolumeDAO volumeDAO;

    @Override
    protected BaseDAO<Workload> getDAO() {
        return dao;
    }

    /**
     * Get workload by id
     *
     * @return
     */
    @Override
    public Workload getById(Long id) throws Exception {
        return dao.getWorkloadById(id);
    }

    /**
     * Get all workloads
     *
     * @return
     */
    @Override
    public List<Workload> getAll() throws Exception {
        return dao.getWorkloads();
    }

    /**
     * Add workloads
     *
     * @param workloads the list of workloads to add
     * @return
     */
    @Override
    public List<Workload> addEntity(List<Workload> workloads) throws Exception {
        return dao.addWorkloads(workloads);
    }

    /**
     * Update workloads
     *
     * @param workloads the list of workloads to update
     * @return
     */
    @Override
    public List<Workload> mergeEntity(List<Workload> workloads) throws Exception {

        //protect against changing ip
        for (Workload w : workloads) {
            Workload existing = this.getById(w.getId());
            if (!existing.getIp().equals(w.getIp())) {
                throw new Exception("You cannot change the workload ip!");
            }
        }

        return dao.updateWorkloads(workloads);
    }

    /**
     * Delete workloads
     *
     * @param workloads the list of workloads to delete
     */
    @Override
    public void deleteEntity(List<Workload> workloads) throws Exception {
        dao.deleteWorkloads(workloads);
    }
}
