package api.service;

import api.dao.BaseDAO;
import api.entity.BaseEntity;

import java.util.List;

public abstract class BaseService<T extends BaseEntity> {

    protected abstract BaseDAO<T> getDAO();

    public abstract T getById(Long id) throws Exception;

    public abstract List<T> getAll() throws Exception;

    public abstract void deleteEntity(List<T> entities) throws Exception;

    public abstract List<T> mergeEntity(List<T> entities) throws Exception;

    public abstract List<T> addEntity(List<T> entities) throws Exception;
}
