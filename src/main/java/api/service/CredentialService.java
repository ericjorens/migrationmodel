package api.service;

import api.dao.BaseDAO;
import api.dao.CredentialDAO;
import api.entity.Credential;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CredentialService extends BaseService<Credential> {
    @EJB
    CredentialDAO dao;

    @Override
    protected BaseDAO<Credential> getDAO() {
        return dao;
    }

    /**
     * Get credential by id
     *
     * @return
     */
    @Override
    public Credential getById(Long id) throws Exception {
        return dao.getCredentialById(id);
    }

    /**
     * Get all credentials
     *
     * @return
     */
    @Override
    public List<Credential> getAll() throws Exception {
        return dao.getCredentials();
    }

    /**
     * Add credentials
     *
     * @param credentials the list of credentials to add
     * @return
     */
    @Override
    public List<Credential> addEntity(List<Credential> credentials) throws Exception {
        return dao.addCredentials(credentials);
    }

    /**
     * Update credentials
     *
     * @param credentials the list of credentials to update
     * @return
     */
    @Override
    public List<Credential> mergeEntity(List<Credential> credentials) throws Exception {
        return dao.updateCredentials(credentials);
    }

    /**
     * Delete credentials
     *
     * @param credentials the list of credentials to delete
     */
    @Override
    public void deleteEntity(List<Credential> credentials) throws Exception {
        dao.deleteCredentials(credentials);
    }

}
