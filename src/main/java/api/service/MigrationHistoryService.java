package api.service;

import api.dao.BaseDAO;
import api.dao.MigrationHistoryDAO;
import api.entity.Migration;
import api.entity.MigrationHistory;
import api.types.MigrationState;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Stateless
public class MigrationHistoryService extends BaseService<MigrationHistory> {
    private long MIGRATION_RUNTIME = 300000;

    @EJB
    MigrationHistoryDAO dao;

    @EJB
    MigrationService migrationService;

    @Override
    protected BaseDAO<MigrationHistory> getDAO() {
        return dao;
    }


    public MigrationHistory getLatestByMigrationId(Long id) throws Exception {
        return dao.getLatestByMigrationId(id);
    }

    public List<MigrationHistory> getAllByMigrationId(Long id) throws Exception {
        return dao.getAllByMigrationId(id);
    }

    /**
     * Start new migration history
     *
     * @param id the migration entity id
     * @return the new migraton history
     * @throws Exception
     */
    public MigrationHistory startMigration(Long id) throws Exception {

        Migration m = this.migrationService.getById(id);
        if (m == null) {
            throw new Exception("There is no migration found!");
        }

        if (m.getState() == MigrationState.RUNNING) {
            throw new Exception("Migration is already running!");
        }

        MigrationHistory mh = new MigrationHistory();
        mh.setStart(new Date());
        m.setState(MigrationState.RUNNING);
        mh.setMigration(m);
        List<MigrationHistory> managedList = dao.addMigrationHistory(Collections.singletonList(mh));

        return managedList.get(0);
    }

    /**
     * End the migration
     *
     * @param id the id of the migration history
     * @return the ended migration history
     * @throws Exception
     */
    public MigrationHistory endMigration(Long id) throws Exception {

        MigrationHistory mh = this.getById(id);
        if (mh == null) {
            throw new Exception("There is no migration history found!");
        }
        if (mh.getMigration().getState() != MigrationState.RUNNING) {
            throw new Exception("Migration is not running!");
        }
        mh.setEnd(new Date());
        mh.setMessage("Migration stopped by user!");
        mh.getMigration().setState(MigrationState.ERROR);

        List<MigrationHistory> managed = dao.updateMigrationHistory(Collections.singletonList(mh));

        if (managed.size() >= 1) {
            return managed.get(0);
        }
        return null;
    }

    /**
     * Get migration by id
     *
     * @return
     */
    @Override
    public MigrationHistory getById(Long id) throws Exception {
        MigrationHistory m = dao.getMigrationHistoryById(id);
        Date date = new Date();
        validateRun(m, date);

        return dao.updateMigrationHistory(Collections.singletonList(m)).get(0);
    }

    /**
     * Get all migration history
     *
     * @return
     */
    public List<MigrationHistory> getAll() throws Exception {
        List<MigrationHistory> mhl = dao.getMigrationHistory();
        Date date = new Date();
        for (MigrationHistory m : mhl) {
            validateRun(m, date);
        }

        return dao.updateMigrationHistory(mhl);
    }

    /**
     * Validate the run. This will simulate the migration
     * @param m the migration history
     * @param d the validation timestamp
     */
    public void validateRun(MigrationHistory m, Date d) {
        if (getElapsedTime(m) >= MIGRATION_RUNTIME && m.getEnd() == null) {
            m.getMigration().setState(MigrationState.SUCCESS);
            m.setEnd(d);
            m.setMessage("Migration was successful!");

            //set the target cloud volumes to the ones that are selected in workload volume
            m.getMigration().getTargetCloud().getWorkload().setVolume(m.getMigration().getVolume());
        }
    }


    public void validateRunForMigrationById(Long migrationId) throws Exception {
        List<MigrationHistory> mhl = this.getAllByMigrationId(migrationId);
        Date date = new Date();
        for (MigrationHistory m : mhl) {
            validateRun(m, date);
        }
    }

    /**
     * Add migrations
     *
     * @param migrations the list of migrations to add
     * @return
     */
    @Override
    public List<MigrationHistory> addEntity(List<MigrationHistory> migrations) throws Exception {
        return dao.addMigrationHistory(migrations);
    }

    /**
     * Update migrations
     *
     * @param migrations the list of migrations to update
     * @return
     */
    @Override
    public List<MigrationHistory> mergeEntity(List<MigrationHistory> migrations) throws Exception {
        return dao.updateMigrationHistory(migrations);
    }

    /**
     * Delete migrations
     *
     * @param migrations the list of migrations to delete
     */
    @Override
    public void deleteEntity(List<MigrationHistory> migrations) throws Exception {
        dao.deleteMigrationHistory(migrations);
    }

    public long getProgressPercentage(MigrationHistory migrationHistory) throws Exception {
        return getElapsedTime(migrationHistory);
    }

    private long getElapsedTime(MigrationHistory m) {
        return Math.abs(m.getStart().getTime() - new Date().getTime());
    }


}
