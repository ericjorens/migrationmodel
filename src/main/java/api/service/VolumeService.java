package api.service;

import api.dao.BaseDAO;
import api.dao.VolumeDAO;
import api.entity.Volume;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class VolumeService extends BaseService<Volume> {
    @EJB
    VolumeDAO dao;

    @Override
    protected BaseDAO<Volume> getDAO() {
        return dao;
    }

    /**
     * Get volume by id
     *
     * @return
     */
    @Override
    public Volume getById(Long id) throws Exception {
        return dao.getVolumeById(id);
    }

    /**
     * Get all volumes
     *
     * @return
     */
    public List<Volume> getAll() throws Exception {
        return dao.getVolumes();
    }

    /**
     * Add volumes
     *
     * @param volumes the list of volumes to add
     * @return
     */
    @Override
    public List<Volume> addEntity(List<Volume> volumes) throws Exception {
        return dao.addVolumes(volumes);
    }

    /**
     * Update volumes
     *
     * @param volumes the list of volumes to update
     * @return
     */
    @Override
    public List<Volume> mergeEntity(List<Volume> volumes) throws Exception {
        return dao.updateVolumes(volumes);
    }

    /**
     * Delete volumes
     *
     * @param volumes the list of volumes to delete
     */
    @Override
    public void deleteEntity(List<Volume> volumes) throws Exception {
        dao.deleteVolumes(volumes);
    }
}
