package api.service;

import api.dao.BaseDAO;
import api.dao.MigrationDAO;
import api.entity.Migration;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class MigrationService extends BaseService<Migration> {
    @EJB
    MigrationDAO dao;
    @EJB
    MigrationHistoryService migrationHistoryService;

    @Override
    protected BaseDAO<Migration> getDAO() {
        return dao;
    }

    /**
     * Get migration by id
     *
     * @return
     */
    @Override
    public Migration getById(Long id) throws Exception {
        //validate all of the history records
        migrationHistoryService.validateRunForMigrationById(id);
        return dao.getMigrationById(id);
    }

    /**
     * Get all migrations
     *
     * @return
     */
    public List<Migration> getAll() throws Exception {
        //validate all of the history records
        List<Migration> migrationList = dao.getMigrations();
        for(Migration m: migrationList){
            migrationHistoryService.validateRunForMigrationById(m.getId());
        }
        return migrationList;
    }

    /**
     * Add migrations
     *
     * @param migrations the list of migrations to add
     * @return
     */
    @Override
    public List<Migration> addEntity(List<Migration> migrations) throws Exception {
        return dao.addMigrations(migrations);
    }

    /**
     * Update migrations
     *
     * @param migrations the list of migrations to update
     * @return
     */
    @Override
    public List<Migration> mergeEntity(List<Migration> migrations) throws Exception {
        return dao.updateMigrations(migrations);
    }

    /**
     * Delete migrations
     *
     * @param migrations the list of migrations to delete
     */
    @Override
    public void deleteEntity(List<Migration> migrations) throws Exception {
        //delete the history first
        for(Migration m: migrations){
            migrationHistoryService.deleteEntity(migrationHistoryService.getAllByMigrationId(m.getId()));
        }
        //delete the migrations
        dao.deleteMigrations(migrations);
    }


}
