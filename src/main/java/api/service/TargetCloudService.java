package api.service;

import api.dao.BaseDAO;
import api.dao.TargetCloudDAO;
import api.entity.TargetCloud;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class TargetCloudService extends BaseService<TargetCloud> {
    @EJB
    TargetCloudDAO dao;

    @Override
    protected BaseDAO<TargetCloud> getDAO() {
        return dao;
    }

    /**
     * Get targetcloud by id
     *
     * @return
     */
    @Override
    public TargetCloud getById(Long id) throws Exception {
        return dao.getTargetCloudById(id);
    }

    /**
     * Get all target clouds
     *
     * @return
     */
    @Override
    public List<TargetCloud> getAll() throws Exception {
        return dao.getTargetClouds();
    }

    /**
     * Add target clouds
     *
     * @param targetClouds the list of target clouds to add
     * @return
     */
    @Override
    public List<TargetCloud> addEntity(List<TargetCloud> targetClouds) throws Exception {
        return dao.addTargetClouds(targetClouds);
    }

    /**
     * Update target clouds
     *
     * @param targetClouds the list of target clouds to update
     * @return
     */
    @Override
    public List<TargetCloud> mergeEntity(List<TargetCloud> targetClouds) throws Exception {
        return dao.updateTargetClouds(targetClouds);
    }

    /**
     * Delete target clouds
     *
     * @param targetClouds the list of target clouds to delete
     */
    @Override
    public void deleteEntity(List<TargetCloud> targetClouds) throws Exception {
        dao.deleteTargetClouds(targetClouds);
    }
}
