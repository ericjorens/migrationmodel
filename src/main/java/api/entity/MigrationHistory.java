package api.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="migration_history")
@NamedQueries({
        @NamedQuery(name = "MigrationHistory.findAll", query = "SELECT m FROM MigrationHistory m"),
        @NamedQuery(name = "MigrationHistory.findAllByMigrationId", query = "SELECT m FROM MigrationHistory m WHERE m.migration.id = :id"),
        @NamedQuery(name = "MigrationHistory.findLatestByMigrationId", query = "SELECT m FROM MigrationHistory m WHERE m.start = (SELECT MAX(mh.start) from MigrationHistory mh WHERE mh.migration.id = :id)")
})
public class MigrationHistory extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    private Migration migration;
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
    @Column
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Migration getMigration() {
        return migration;
    }

    public void setMigration(Migration migration) {
        this.migration = migration;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MigrationHistory that = (MigrationHistory) o;

        if (!id.equals(that.id)) return false;
        if (!migration.equals(that.migration)) return false;
        if (!start.equals(that.start)) return false;
        if (end != null ? !end.equals(that.end) : that.end != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + migration.hashCode();
        result = 31 * result + start.hashCode();
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
