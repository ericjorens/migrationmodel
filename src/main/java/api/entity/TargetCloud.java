package api.entity;

import api.types.CloudType;

import javax.persistence.*;


/**
 * author: Eric Jorens
 * date: March 2020
 */
@Entity
@NamedQueries({
        @NamedQuery(name="TargetCloud.findAll", query="SELECT tc FROM TargetCloud tc")
})
public class TargetCloud extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    @Column
    private CloudType cloudType;
    @OneToOne
    private Credential credential;
    @OneToOne
    private Workload workload;

    public TargetCloud() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CloudType getCloudType() {
        return cloudType;
    }

    public void setCloudType(CloudType cloudType) {
        this.cloudType = cloudType;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential cloudCredentials) {
        this.credential = cloudCredentials;
    }

    public Workload getWorkload() {
        return workload;
    }

    public void setWorkload(Workload workload) {
        this.workload = workload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TargetCloud that = (TargetCloud) o;

        if (!id.equals(that.id)) return false;
        if (cloudType != that.cloudType) return false;
        if (!credential.equals(that.credential)) return false;
        return workload.equals(that.workload);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + cloudType.hashCode();
        result = 31 * result + credential.hashCode();
        result = 31 * result + workload.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TargetCloud{" +
                "id=" + id +
                ", cloudType=" + cloudType +
                ", credential=" + credential +
                ", workload=" + workload +
                '}';
    }
}
