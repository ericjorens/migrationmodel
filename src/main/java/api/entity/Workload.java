package api.entity;


import javax.persistence.*;
import java.util.List;

/**
 * author: Eric Jorens
 * date: March 2020
 */
@Entity
@NamedQueries({
        @NamedQuery(name="Workload.findAll", query="SELECT w FROM Workload w")
})
public class Workload extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String ip;
    @OneToOne
    private Credential credential;
    @OneToMany(targetEntity = Volume.class, cascade = CascadeType.MERGE)
    private List<Volume> volume;

    public Workload() {
    }

    public Workload(String ip, Credential credential, List<Volume> volumes) {
        this.ip = ip;
        this.credential = credential;
        this.volume = volumes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credentials) {
        this.credential = credentials;
    }

    public List<Volume> getVolume() {
        return volume;
    }

    public void setVolume(List<Volume> volume) {
        this.volume = volume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Workload workload = (Workload) o;

        if (!id.equals(workload.id)) return false;
        if (!ip.equals(workload.ip)) return false;
        if (!credential.equals(workload.credential)) return false;
        return volume.equals(workload.volume);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + ip.hashCode();
        result = 31 * result + credential.hashCode();
        result = 31 * result + volume.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Workload{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                ", credential=" + credential +
                ", volume=" + volume +
                '}';
    }
}
