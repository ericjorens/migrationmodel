package api.entity;

import javax.persistence.*;


/**
 * author: Eric Jorens
 * date: March 2020
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Volume.findAll", query = "SELECT v FROM Volume v")
})
public class Volume extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String mountPoint;
    @Column
    private int size;

    public Volume() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMountPoint() {
        return mountPoint;
    }

    public void setMountPoint(String mountPoint) {
        this.mountPoint = mountPoint;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Volume volume = (Volume) o;

        if (size != volume.size) return false;
        if (!id.equals(volume.id)) return false;
        return mountPoint.equals(volume.mountPoint);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + mountPoint.hashCode();
        result = 31 * result + size;
        return result;
    }

    @Override
    public String toString() {
        return "Volume{" +
                "id=" + id +
                ", mountPoint='" + mountPoint + '\'' +
                ", size=" + size +
                '}';
    }
}
