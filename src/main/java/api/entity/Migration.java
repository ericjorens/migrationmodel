package api.entity;

import api.types.MigrationState;

import javax.persistence.*;
import java.util.List;

/**
 * author: Eric Jorens
 * date: March 2020
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Migration.findAll", query = "SELECT m FROM Migration m")
})
public class Migration extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(targetEntity = Volume.class, cascade = CascadeType.MERGE)
    private List<Volume> volume;
    @OneToOne
    private Workload workload;
    @OneToOne
    private TargetCloud targetCloud;
    @Enumerated(EnumType.STRING)
    @Column
    private MigrationState state;

    public Migration() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Volume> getVolume() {
        return volume;
    }

    public void setVolume(List<Volume> mountPoints) {
        this.volume = mountPoints;
    }

    public Workload getWorkload() {
        return workload;
    }

    public void setWorkload(Workload workload) {
        this.workload = workload;
    }

    public TargetCloud getTargetCloud() {
        return targetCloud;
    }

    public void setTargetCloud(TargetCloud targetCloud) {
        this.targetCloud = targetCloud;
    }

    public MigrationState getState() {
        return state;
    }

    public void setState(MigrationState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Migration migration = (Migration) o;

        if (!id.equals(migration.id)) return false;
        if (!volume.equals(migration.volume)) return false;
        if (!workload.equals(migration.workload)) return false;
        if (!targetCloud.equals(migration.targetCloud)) return false;
        return state == migration.state;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + volume.hashCode();
        result = 31 * result + workload.hashCode();
        result = 31 * result + targetCloud.hashCode();
        result = 31 * result + state.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Migration{" +
                "id=" + id +
                ", mountPoints='" + volume + '\'' +
                ", workload=" + workload +
                ", targetCloud=" + targetCloud +
                ", state=" + state +
                '}';
    }
}
