package api.dao;

import api.entity.Credential;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CredentialDAO extends BaseDAO<Credential> {

    /**
     * Find a credential
     *
     * @param id the id of the credential
     * @return
     */
    public Credential getCredentialById(Long id) throws Exception {
        return this.findById(id, Credential.class);
    }

    /**
     * Get all credentials
     *
     * @return
     */
    public List<Credential> getCredentials() throws Exception {
        return this.getAll("Credential.findAll");
    }


    /**
     * Add credential(s)
     *
     * @param credentials the list of credentials to add
     * @return
     */
    public List<Credential> addCredentials(List<Credential> credentials) throws Exception {
        return this.save(credentials);
    }

    /**
     * Update credential(s)
     *
     * @param credentials the list of credentials to update
     * @return
     */
    public List<Credential> updateCredentials(List<Credential> credentials) throws Exception {
        return this.merge(credentials);
    }

    /**
     * Delete credential(s)
     *
     * @param credentials the list of credentials to delete
     */
    public void deleteCredentials(List<Credential> credentials) throws Exception {
        this.delete(credentials);
    }
}
