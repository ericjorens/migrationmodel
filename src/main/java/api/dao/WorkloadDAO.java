package api.dao;

import api.entity.Workload;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class WorkloadDAO extends BaseDAO<Workload> {

    /**
     * Find a workload
     *
     * @param id the id of the workload
     * @return
     */
    public Workload getWorkloadById(Long id) throws Exception {
        return this.findById(id, Workload.class);
    }

    /**
     * Get all workloads
     *
     * @return
     */
    public List<Workload> getWorkloads() throws Exception {
        return this.getAll("Workload.findAll");
    }

    /**
     * Add workload(s)
     *
     * @param workloads the list of workloads to add
     * @return
     */
    public List<Workload> addWorkloads(List<Workload> workloads) throws Exception {
        return this.save(workloads);
    }

    /**
     * Update workload(s)
     *
     * @param workloads the list of workloads to update
     * @return
     */
    public List<Workload> updateWorkloads(List<Workload> workloads) throws Exception {
        return this.merge(workloads);
    }

    /**
     * Delete workload(s)
     *
     * @param workloads the list of workloads to delete
     */
    public void deleteWorkloads(List<Workload> workloads) throws Exception {
            this.delete(workloads);
    }
}
