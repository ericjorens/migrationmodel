package api.dao;

import api.entity.Migration;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class MigrationDAO extends BaseDAO<Migration> {
    /**
     * Find a migration
     *
     * @param id the id of the migration
     * @return
     */
    public Migration getMigrationById(Long id) throws Exception {
        return this.findById(id, Migration.class);
    }

    /**
     * Get all migrations
     *
     * @return
     */
    public List<Migration> getMigrations() throws Exception {
        return this.getAll("Migration.findAll");
    }

    /**
     * Add migration(s)
     *
     * @param migrations the list of migrations to add
     * @return
     */
    public List<Migration> addMigrations(List<Migration> migrations) throws Exception {
        return this.save(migrations);
    }

    /**
     * Update migration(s)
     *
     * @param migrations the list of migrations to update
     * @return
     */
    public List<Migration> updateMigrations(List<Migration> migrations) throws Exception {
        return this.merge(migrations);
    }

    /**
     * Delete migration(s)
     *
     * @param migrations the list of migrations to delete
     */
    public void deleteMigrations(List<Migration> migrations) throws Exception {
        this.delete(migrations);
    }
}
