package api.dao;

import api.entity.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseDAO<T extends BaseEntity> {

    @PersistenceContext(unitName = "migration")
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public <T> List<T> getAll(String query) throws Exception {
        try {
            return entityManager.createNamedQuery(query).getResultList();
        } catch (PersistenceException e) {
            throw new Exception(e);
        }
    }

    /**
     * Save an entity.
     *
     * @param entityList
     * @return the saved entity list
     */
    public <T> List<T> save(List<T> entityList) throws Exception {
        try {
            List<T> added = new ArrayList<T>();
            for (T t : entityList) {
                entityManager.persist(t);
                added.add(t);
            }
            entityManager.flush();
            return added;
        } catch (PersistenceException e) {
            throw new Exception(e);
        }

    }

    /**
     * merge an entity.
     *
     * @param entityList
     * @return the saved entity
     */
    public <T> List<T> merge(List<T> entityList) throws Exception {
        try {
            List<T> updated = new ArrayList<T>();
            for (T t : entityList) {
                updated.add(entityManager.merge(t));
            }
            entityManager.flush();
            return updated;
        } catch (PersistenceException e) {
            throw new Exception(e);
        }

    }

    /**
     * Returns a reference to the entity with the given identifier.
     *
     * @param id
     * @return
     */
    public T findById(Long id, Class clazz) throws Exception {
        try {
            T t = (T) entityManager.find(clazz, id);
            entityManager.flush();
            return t;
        } catch (PersistenceException e) {
            throw new Exception(e);
        }

    }

    /**
     * Delete an entity
     *
     * @param entityList the list of entities to remove
     */
    public void delete(List<T> entityList) throws Exception {
        try {
            List<T> managedEntities = this.merge(entityList);
            for(T e: managedEntities){
                entityManager.remove(e);
            }
        } catch (PersistenceException e) {
            throw new Exception(e);
        }

    }

    public void close() throws Exception {
        try {
            entityManager.clear();
            entityManager.close();
        } catch (PersistenceException e) {
            throw new Exception(e);
        }

    }
}
