package api.dao;

import api.config.DBConstants;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class JpaEntityManager {
    private EntityManagerFactory emFactoryObj;
    private final String PERSISTENCE_UNIT_NAME = "migration";
    private final Map<String, String> properties=new HashMap<String, String>();

    public EntityManager getEntityManager() {
        String url = String.format("%s/%s", DBConstants.DB_HOST, DBConstants.DB_NAME);
        properties.put("javax.persistence.jdbc.driver",DBConstants.DB_DRIVER);
        properties.put("javax.persistence.jdbc.url", url);
        properties.put("javax.persistence.jdbc.user", DBConstants.DB_USER);
        properties.put("javax.persistence.jdbc.password",DBConstants.DB_PASSWORD);
        properties.put("useSSL", DBConstants.DB_USE_SSL);
        properties.put("requireSSL", DBConstants.DB_REQUIRED_SSL);
        emFactoryObj = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);
        return emFactoryObj.createEntityManager();
    }
}
