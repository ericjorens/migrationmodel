package api.dao;

import api.entity.MigrationHistory;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;


@Stateless
public class MigrationHistoryDAO extends BaseDAO<MigrationHistory> {

    /**
     * get the latest history for a migration
     * @param id the migration id
     * @return a migration history
     * @throws Exception
     */
    public MigrationHistory getLatestByMigrationId(Long id) throws Exception {
        try {
            Query q = getEntityManager().createNamedQuery("MigrationHistory.findLatestByMigrationId");
            q.setParameter("id", id);

            MigrationHistory m = (MigrationHistory) q.getSingleResult();
            getEntityManager().flush();

            return m;
        } catch (PersistenceException e) {
            throw new Exception(e);
        }

    }

    /**
     * get all migration history for a migration
     * @param id the migration id
     * @return the list of migration history
     * @throws Exception
     */
    public List<MigrationHistory> getAllByMigrationId(Long id) throws Exception {
        try {
            Query q = getEntityManager().createNamedQuery("MigrationHistory.findAllByMigrationId");
            q.setParameter("id", id);
            List<MigrationHistory> mhl = (List<MigrationHistory>) q.getResultList();
            getEntityManager().flush();
            return mhl;
        } catch (PersistenceException e) {
            throw new Exception(e);
        }
    }

    /**
     * Find a migration history by id
     *
     * @param id the id of the migration history
     * @return
     */
    public MigrationHistory getMigrationHistoryById(Long id) throws Exception {
        return this.findById(id, MigrationHistory.class);
    }

    /**
     * Get all migration history
     *
     * @return
     */
    public List<MigrationHistory> getMigrationHistory() throws Exception {
        return this.getAll("MigrationHistory.findAll");
    }

    /**
     * Add migration history
     *
     * @param migrations the list of migration history to add
     * @return
     */
    public List<MigrationHistory> addMigrationHistory(List<MigrationHistory> migrations) throws Exception {
        return this.save(migrations);
    }

    /**
     * Update migration history
     *
     * @param migrations the list of migration history to update
     * @return
     */
    public List<MigrationHistory> updateMigrationHistory(List<MigrationHistory> migrations) throws Exception {
        return this.merge(migrations);
    }

    /**
     * Delete migration history
     *
     * @param migrations the list of migration history to delete
     */
    public void deleteMigrationHistory(List<MigrationHistory> migrations) throws Exception {
        this.delete(migrations);
    }
}
