package api.dao;

import api.entity.TargetCloud;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class TargetCloudDAO extends BaseDAO<TargetCloud> {

    /**
     * Find a targetcloud
     *
     * @param id the id of the targetcloud
     * @return
     */
    public TargetCloud getTargetCloudById(Long id) throws Exception {
        return this.findById(id, TargetCloud.class);
    }

    /**
     * Get all target clouds
     *
     * @return
     */
    public List<TargetCloud> getTargetClouds() throws Exception {
        return this.getAll("TargetCloud.findAll");
    }

    /**
     * Add targetcloud(s)
     *
     * @param targetClouds the list of targetClouds to add
     * @return
     */
    public List<TargetCloud> addTargetClouds(List<TargetCloud> targetClouds) throws Exception {
        return this.save(targetClouds);
    }

    /**
     * Update targetcloud(s)
     *
     * @param targetClouds the list of targetClouds to update
     * @return
     */
    public List<TargetCloud> updateTargetClouds(List<TargetCloud> targetClouds) throws Exception {
        return this.merge(targetClouds);
    }

    /**
     * Delete targetcloud(s)
     *
     * @param targetClouds the list of targetClouds to delete
     */
    public void deleteTargetClouds(List<TargetCloud> targetClouds) throws Exception {
        this.delete(targetClouds);
    }
}
