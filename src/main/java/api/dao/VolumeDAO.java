package api.dao;

import api.entity.Volume;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class VolumeDAO extends BaseDAO<Volume> {

    /**
     * Find a volume
     *
     * @param id the id of the volume
     * @return
     */
    public Volume getVolumeById(Long id) throws Exception {
        return this.findById(id, Volume.class);
    }

    /**
     * Get all volumes
     *
     * @return
     */
    public List<Volume> getVolumes() throws Exception {
        return this.getAll("Volume.findAll");
    }

    /**
     * Add volume(s)
     *
     * @param volumes the list of volumes to add
     * @return
     */
    public List<Volume> addVolumes(List<Volume> volumes) throws Exception {
        return this.save(volumes);
    }

    /**
     * Update volume(s)
     *
     * @param volumes the list of volumes to update
     * @return
     */
    public List<Volume> updateVolumes(List<Volume> volumes) throws Exception {
        return this.merge(volumes);
    }

    /**
     * Delete volume(s)
     *
     * @param volumes the list of volumes to delete
     */
    public void deleteVolumes(List<Volume> volumes) throws Exception {
        this.delete(volumes);
    }
}