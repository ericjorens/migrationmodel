package api.types;

public enum MigrationState {
    NOT_STARTED,
    RUNNING,
    ERROR,
    SUCCESS
}
