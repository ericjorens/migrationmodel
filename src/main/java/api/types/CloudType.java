package api.types;

/**
 * author: Eric Jorens
 * date: March 2020
 */
public enum CloudType {
    AWS,
    AZURE,
    VSPHERE,
    VCLOUD
}
