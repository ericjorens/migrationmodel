-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: migration_model
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `credential`
--

LOCK TABLES `credential` WRITE;
/*!40000 ALTER TABLE `credential` DISABLE KEYS */;
INSERT INTO `credential` VALUES (11,'Bill','password','TNT'),(12,'Ted','pumpkin','TNT'),(13,'Joe','yesman','NPM'),(16,'Mark','password','NPM'),(29,'Ken','letmein','WWE'),(30,'Alex','testing','WWE');
/*!40000 ALTER TABLE `credential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES (23,23,7,'NOT_STARTED'),(24,24,9,'NOT_STARTED');
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migration_history`
--

LOCK TABLES `migration_history` WRITE;
/*!40000 ALTER TABLE `migration_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migration_volume`
--

LOCK TABLES `migration_volume` WRITE;
/*!40000 ALTER TABLE `migration_volume` DISABLE KEYS */;
INSERT INTO `migration_volume` VALUES (66,23,5),(67,23,8),(68,24,5);
/*!40000 ALTER TABLE `migration_volume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `targetcloud`
--

LOCK TABLES `targetcloud` WRITE;
/*!40000 ALTER TABLE `targetcloud` DISABLE KEYS */;
INSERT INTO `targetcloud` VALUES (7,12,21,'AWS'),(8,11,23,'AWS'),(9,11,23,'VSPHERE');
/*!40000 ALTER TABLE `targetcloud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `volume`
--

LOCK TABLES `volume` WRITE;
/*!40000 ALTER TABLE `volume` DISABLE KEYS */;
INSERT INTO `volume` VALUES (1,'D:\\',500),(2,'EXTERNAL:\\',500),(3,'F:\\',650),(4,'/usr/var/lib',100),(5,'C:\\',500),(8,'W:\\',400),(10,'K:\\',500),(11,'Q:\\',256);
/*!40000 ALTER TABLE `volume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workload`
--

LOCK TABLES `workload` WRITE;
/*!40000 ALTER TABLE `workload` DISABLE KEYS */;
INSERT INTO `workload` VALUES (21,12,'222.333.444'),(23,11,'999.444.555'),(24,13,'666.777.889'),(25,16,'888.999.000');
/*!40000 ALTER TABLE `workload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workload_volume`
--

LOCK TABLES `workload_volume` WRITE;
/*!40000 ALTER TABLE `workload_volume` DISABLE KEYS */;
INSERT INTO `workload_volume` VALUES (76,21,1),(84,24,2),(85,24,5),(104,23,5),(105,23,8),(106,23,3);
/*!40000 ALTER TABLE `workload_volume` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-09 10:47:53
