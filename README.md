# README #

This is a sample Java API project, designed to illustrate the basic workings of a data migration tool.

### Getting Started ###
1. Download and Install Java 8 from https://www.oracle.com/java/technologies/javase-jdk8-downloads.html

2. Download and Install MySQL Community (or other) from https://dev.mysql.com/downloads/installer/
    1.  Install MySQL and MySQL Workbench using the installer
    
3. Install the SQL model and test data
    1. Open the project file "MigrationModel/sql/migration_model.mwb" using MySQL Workbench
    2. Perform a schema synch by selecting Database > Synchronize Model... and follow the prompts
    3. Import the test data by running the "MigrationModel/sql/test_data.sql" script against the migration_model schema
    
4. Install and Configure Glassfish 5.0
    1. Download the "glassfish-5.0.zip" from https://download.oracle.com/glassfish/5.0/release/index.html
    2. Unzip the package and place the dir into your dev environment, i.e. C:\
    3. Copy the MySql jdbc driver "mysql-connector-java-5.1.48-bin.jar" from MigrationModel\sql into your Glassfish library 
    "[glassfish_install_path]\glassfish-5.0\glassfish5\glassfish\lib"
    4. Add the following Windows system variables:
        * GLASSFISH_HOME : "[glassfish_install_path]\glassfish-5.0\glassfish5\glassfish\bin"
        * JAVA_HOME : "[java_install_path]\Java\jdk1.8.0[patch_version]\bin"
    5. Open a cmd (as admin), into the glassfish bin dir "[glassfish_install_path]\glassfish-5.0\glassfish5\glassfish\bin"
    6. Start the default domain with the command "asadmin start-domain"
    7. Open the Glassfish admin panel in a browser by going to http://localhost:4848/
 
 5. Configure the Glassfish JDBC Connection Pool
    1. Select JDBC > JDBC Connection Pools from the Glassfish Admin navigation panel
    2. Add a new connection pool with the following settings:
        * Pool Name : "MigrationPool" (This name is optional but must be selected as the JDBC Resource pool)
        * Resource Type: "java.sql.Driver"
        * Driver Classname: "com.mysql.jdbc.Driver"
        1. Add the following additional Properties to the connection pool:
            * "user" : "migrator" (This user should have been added in the SQL synchronization)
            * "passowrd" : "migrator" (This password should have been added in the SQL synchronization)
            * "URL" : "jdbc:mysql://localhost:3306/migration_model" (This schema should have been added in the SQL synchronization)
            * "useSSL" : "false"
            * "autoReconnect" : "true"
            
 6. Configure the Glassfish JDBC Resource
    1. Select JDBC > JDBC Resources from the Glassfish Admin navigation panel
    2. Add a new connection pool with the following properties
        * JNDI Name : "jdbc/migrationDS" (This MUST be the name, as it is used to configure the API persistence)
        * Pool Name : "MigrationPool" (Or whatever you called the connection pool from above)

7. Configure the EJB Settings
    1. Select Configurations > server-config > EJB Container > EJB Timer Service from the Glassfish Admin navigation panel
    2. Set the "Timer Datasource" to "jdbc/migrationDS" (same as the JNDI Name)
    
8. Configure the Glassfish JVM Settings to give the API more computing power
    1. Select Configurations > server-config > JVM Settings > JVM Options from the Glassfish Admin navigation panel
    2. Set the following JVM Options:
        * "-XX:MaxPermSize=512m"
        * "-Xmx1024m"
        
9. Stop the Glassfish Server:
    1. Open a cmd (as admin), into the glassfish bin dir "[glassfish_install_path]\glassfish-5.0\glassfish5\glassfish\bin"
    2. Start the default domain with the command "asadmin stop-domain"
        
10. Configure your IDE to run the app. The following are basic steps that may differ depending on your choice of IDE:
    * Create a new run configuration to deploy to Glassfish Server
    * Set the server domain to "domain1" (set the user/password to whatever you set in Glassfish Admin, default is null)
    * Set the project JRE to 1.8
    * Set your after launch to open a browser to http://localhost:8080 (this is optional)
    * Set the prelaunch to build the artifact "MigrationModel:war exploded"
    * Set the deployment artifact to "MigrationModel:war exploded"
    
11. Deploy the API
    * Run the Maven install project task (this assumes you have already installed Maven and it is configured in your IDE)
    * Run your Glassfish deployment (as configured above)
    * Navigate to http://localhost:8080
    
    
    


